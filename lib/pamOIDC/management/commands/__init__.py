# -*- coding: utf-8 -*-
'''
/**
 * Copyright 2013 Nomura Research Institute, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'''

from django.core.management.base import BaseCommand

from optparse import make_option
import sys


class GenericCommand(BaseCommand):
    args = ''
    help = ''
    model = None

    option_list = BaseCommand.option_list + (
        make_option('--encoding', '-e',
                    action='store',
                    dest='encoding',
                    default='utf-8',
                    help=u'encoding'),
    )
    ''' Command Option '''

    def open_file(self, options):
        fp = sys.stdin if options['file'] == 'stdin' else open(options['file'])
        return fp

    def command_help(self, *args, **options):
        import re
        for i in dir(self):
            m = re.search('^command_(.*)$', i)
            if m is None:
                continue
            print m.group(1)
        print args
        print options

    def handle(self, *args, **options):
        '''  command main '''

        if len(args) < 1:
            return "a sub command must be specfied"
        self.command = args[0]
        getattr(self,
                'command_%s' % self.command,
                GenericCommand.command_help)(*args[1:], **options)
