# -*- coding: utf-8 -*-
'''
/**
 * Copyright 2013 Nomura Research Institute, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'''

from . import GenericCommand
from ... import auth
import shutil
import os


class Command(GenericCommand):

    def command_create(self, app='app', *args, **options):
        shutil.copy(auth.__file__.replace('pyc', 'py'), '%s/' % app)
        with open("%s/env.py" % app, "w") as out:
            out.write("APP='%s'\n" % app)
            ve = os.environ.get("VIRTUAL_ENV", None)
            ve = ve and "'%s'" % ve or 'None'
            out.write("VE=%s\n" % ve)
            with open(auth.__file__.replace("auth.pyc", "env.py")) as src:
                out.write(src.read())

    def command_conf(self, app='app', handler_class='auth.Django',
                     *args, **options):
        print "# -- /etc/pam.d/{{ service name }}"
        modname = os.path.join(os.path.abspath("."), "%s/auth.py" % app,)

        print "auth sufficient pam_python.so %s %s" % (modname, handler_class)
        print "account    required pam_permit.so"
