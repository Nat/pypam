.. OpenID Sandbox documentation master file, created by
   sphinx-quickstart on Tue Nov  5 10:03:28 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pamOIDC Documentation
==========================================

This is an open source implementation of 
functions OpenID Connect in Python by Nomura Research Institute, Ltd. 

This project was partly funded by the PEOFIAMP project 
sponsored by the Ministry of internal affairs and communication of Japan 
as a part of EU-Japan cooperation funding and is offered as the result of the project.

Contents:

.. toctree::
    :maxdepth: 2


    setup
    sasl_auth
    connect_ruby
    introspect

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

