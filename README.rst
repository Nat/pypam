========================================================================
pamOIDC-python
========================================================================

About
=========

This is an open source implementation of 
functions OpenID Connect in Python by Nomura Research Institute, Ltd. 

This project was partly funded by the PEOFIAMP project 
sponsored by the Ministry of internal affairs and communication of Japan 
as a part of EU-Japan cooperation funding and is offered as the result of the project.

License
===========

The project is offered under Apache license 2.0.

::

    Copyright 2013 Nomura Research Institute, Ltd.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    

Requirements
============

PYPI
--------

Detailed information about the minimum supported Django version and 
other Python modules that may be required in order to run this software is shown below:

.. literalinclude:: ../requirements.txt

This information exists in the ``requirements.txt`` file 
inside the ** lots ** distribution package. 
If ``pip`` is used to install this software,
then all these dependencies will also be installed, 
if they are not already installed in your system.

libpam-python
----------------

This software depends on pam_python:

    - http://ace-host.stuart.id.au/russell/files/pam_python/

For Debian/Ubuntu:

::

    $ sudo apt-get install libpam-python

Otherwise, install it with tarball.

Install
==============

To install **pamOIDC-python** from soruce code, use the provided installation script::

    python setup.py install


Or it is also possible to install this application directly from
the `source code repository`_ using ``pip``::

    pip install -e hg+https://bitbucket.org/PEOFIAMP/pamOIDC-python#egg=pamOIDC-python

The above command will install the latest development release of **pamOIDC-python**.


How to build the documentation
================================

This project's documentation is located in source form under the ``docs``
directory. In order to convert the documentation to a format that is
easy to read and navigate you need the ``sphinx`` package.

You can install ``sphinx`` using ``pip``::

    pip install sphinx

Or ``easy_install``::

    easy_install sphinx

Once ``sphinx`` is installed, change to the ``docs`` directory, open a shell
and run the following command::

    make html

This will build a HTML version of the documentation. You can read the
documentation by opening the following file in any web browser::

    docs/_build/html/index.html


