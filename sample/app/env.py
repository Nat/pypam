'''
/**
 * Copyright 2013 Nomura Research Institute, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'''

APP = 'app'
VE = '/home/hdknr/ve/slu'
import os
import sys


def init():
    if VE:
        activate_this = "%s/bin/activate_this.py" % VE
        execfile(activate_this, dict(__file__=activate_this))

    PRJ_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    sys.path.insert(0, PRJ_PATH)
    sys.path.insert(0, os.path.join(PRJ_PATH, APP))
    os.environ['DJANGO_SETTINGS_MODULE'] = '%s.settings' % APP
