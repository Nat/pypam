# -*- coding: utf-8 -*-
'''
/**
 * Copyright 2013 Nomura Research Institute, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'''

from django.core.management.base import BaseCommand
from optparse import make_option
from ...models import OpenIds


class Command(BaseCommand):
    args = ''
    help = ''
    option_list = BaseCommand.option_list + (
        make_option(
            '--charset',
            action='store',
            dest='charset',
            default='utf8',
            help='end record'),
    )

    def handle(self, *args, **options):
        if len(args) > 0:
            getattr(
                self, 'handle_%s' % args[0], self.handle_help
            )(*args, **options)
        else:
            self.handle_help(*args, **options)

    def handle_help(self, *args, **options):
        print args, options

    ####
    def handle_openid(self, *args, **options):
        print "# model id, ppid, access token"
        for t in OpenIds.objects.all():
            print t.id, t.identifier, t.access_token

    def handle_introspect_id_token(self, *args, **options):
        ''' Call introspect endpoint to check token is valid or not '''
        if len(args) > 1:
            print OpenIds.objects.get(id=args[1]).introspect_id_token()
            return

        print OpenIds.objects.all()[0].introspect_id_token()

    def handle_introspect_access_token(self, *args, **options):
        ''' Call introspect endpoint to check token is valid or not '''
        if len(args) > 1:
            print OpenIds.objects.get(id=args[1]).introspect_access_token()
            return

        print OpenIds.objects.all()[0].introspect_access_token()
